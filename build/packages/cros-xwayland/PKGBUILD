# Maintainer: David Riley <davidriley@chromium.org>
# Maintainer: Chloe Pelling <cpelling@google.com>

pkgname=cros-xwayland
pkgver=23.2.5
pkgrel=1
arch=('x86_64')
license=('custom')
groups=('xorg')
url="https://xorg.freedesktop.org"
pkgdesc="xwayland server"
depends=('nettle' 'libepoxy' 'systemd-libs' 'libxfont2'
         'pixman' 'xorg-server-common' 'libxcvt')
makedepends=('meson' 'xorgproto' 'xtrans' 'libxkbfile' 'dbus'
             'xorg-font-util'
             'wayland' 'wayland-protocols'
             'libdrm' 'mesa-libgl'
             'systemd'
             'egl-wayland'
)
source=(https://commondatastorage.googleapis.com/chromeos-mirror/gentoo/distfiles/xwayland-$pkgver.tar.xz
        '0001-xwayland-virtwl-with-dmabuf-for-1.20.1.patch'
        'use-gbm_bo_use_scanout.patch'
        'xwayland-Support-zink-on-venus-enabled-as-driver.patch')
sha512sums=('8cf90d51db7c4c7e8191b5919e6529fe577bc735bb1d00fbc0139cb67ef95d30eb56c2026f3b2f639745c6a621b33bbd60f9ebabdd12175148dd192393ce1169'
            'd275c7ebe504c5f136e4316a35ff9dc4bb42b34b219d80d5d0327ee6f5855ad1a1320c495fa7aac7c2ced590d1a7a0a268b01fff41ee47f741a3f9e01ce1295f'
            '82068015568b04dc8634e70836997494c04f885740d1167326b545db2bf02c026e6fcf41518c9825079c9dba1defe0a754da4a2f600cb8f9755f76fa68121edc'
            'd30d1a0c2d200e728b15ebe8e6c01bdba9cfaeefc989743bfe3cdd0a169fca383855970ee91f797a1c0a498fe6350cb1710a4b7d8f9098c00aca22409697798d')
provides=('cros-xwayland' 'xorg-server-xwayland' 'xorg-xwayland')
conflicts=('cros-xwayland' 'xorg-server-xwayland' 'xorg-xwayland')
replaces=('cros-xwayland' 'xorg-server-xwayland' 'xorg-xwayland')
validpgpkeys=('67DC86F2623FC5FD4BB5225D14706DBE1E4B4540') # "Olivier Fourdan <fourdan@xfce.org>"
options=(debug strip)

prepare() {
  cd "xwayland-$pkgver"
  patch --forward --strip=1 --input="${srcdir}/0001-xwayland-virtwl-with-dmabuf-for-1.20.1.patch"
  patch --forward --strip=1 --input="${srcdir}/use-gbm_bo_use_scanout.patch"
  patch --forward --strip=1 --input="${srcdir}/xwayland-Support-zink-on-venus-enabled-as-driver.patch"
}

build() {
  arch-meson xwayland-$pkgver build \
    -D ipv6=true \
    -D xvfb=false \
    -D xdmcp=false \
    -D xcsecurity=true \
    -D dri3=true \
    -D xwayland_eglstream=true \
    -D glamor=true \
    -D xkb_dir=/usr/share/X11/xkb \
    -D xkb_output_dir=/var/lib/xkb

  meson configure build
  ninja -C build
}

package() {
  install -m755 -Dt "${pkgdir}"/usr/bin build/hw/xwayland/Xwayland
  install -m644 -Dt "${pkgdir}"/usr/share/man/man1 build/hw/xwayland/Xwayland.1
  install -m644 -Dt "${pkgdir}"/usr/lib/pkgconfig build/meson-private/xwayland.pc

  install -m644 -Dt "${pkgdir}/usr/share/licenses/${pkgname}" xwayland-$pkgver/COPYING
}
