# Borealis unit tests

When `tools/build_full.py` is called with the `--run-tests` flag, this folder is
mounted as /test and `/test/run_tests` is executed.

To add new tests, create a new `test_*.py` file in here.
