# swap tools

This directory is for scripts that affect swap (disk, zram) and swap-related
systems (zram writeback).

- configure_swap.py: Set up disk-based swap, zram writeback etc for memory
  testing using tast and benchy.
