# Copyright 2022 The ChromiumOS Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

"""Common configuration values."""

# Default Docker tag for built Borealis images
BOREALIS_TAG = "cros_borealis"
